package MyFirstAppiumProject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class First {
    public static void main(String[] args) throws Exception {

        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName", "Android");
        desiredCapabilities.setCapability("udid", "674290fd");
        desiredCapabilities.setCapability("platformVersion", "10.0");
        desiredCapabilities.setCapability("automationName", "UiAutomator2");
//        desiredCapabilities.setCapability("noReset", true);
        desiredCapabilities.setCapability("appPackage", "com.instagram.android");
        desiredCapabilities.setCapability("appActivity", "com.instagram.mainactivity.MainActivity");
        desiredCapabilities.setCapability("ignoreHiddenApiPolicyError", true);
//        desiredCapabilities.setCapability("appWaitActivity", "com.instagram.nux.activity.SignedOutFragmentActivity");


        URL url = new URL("http://127.0.0.1:6432/wd/hub");

        AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(url, desiredCapabilities);
        String sessionId = driver.getSessionId().toString();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // Find Profile Icon
//        AndroidElement Profile_Tab = driver.findElementById("com.instagram.android:id/tab_avatar");
        // Long press it
        AndroidTouchAction action = new AndroidTouchAction(driver);
//        action.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(Profile_Tab))).perform();


        // com.instagram.android:id/row_user_textview :: Add Account
//        AndroidElement Add = driver.findElementByXPath("//android.widget.TextView[@text='Add account']");
//        Add.click();

        // com.instagram.android:id/primary_button :: Log in button(blue)
//        AndroidElement Log_in_blue = driver.findElementById("com.instagram.android:id/primary_button");
//        Log_in_blue.click();


        // com.instagram.nux.activity.SignedOutFragmentActivity
        driver.findElement(By.id("com.instagram.android:id/log_in_button")).click();

        // com.instagram.android:id/login_username
        AndroidElement UserName = driver.findElement(By.id("com.instagram.android:id/login_username"));
        UserName.sendKeys("pic_uptest");

        AndroidElement PassWord = driver.findElement(By.id("com.instagram.android:id/password"));
        PassWord.sendKeys("orhan1234");

        driver.findElementById("com.instagram.android:id/button_text").click();

        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("com.instagram.android:id/tab_avatar")));

        Thread.sleep(5000);
        driver.quit();
    }
}
